/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <statistical calculator> 
 * @brief <This application performs statistical analytics on a dataset >
 *
 * <with the use of this application you can calculate the mean,median,maximum and minimum values of a dataset and also you can sort the dataset from the maximum value to the munimum value>
 *
 * @author <Muhammad Yehia>
 * @date <7/12/2018 >
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE 40

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  
  /* Other Variable Declarations Go Here */

  /* Statistics and Printing Functions Go Here */
 print_array(test,SIZE);
 print_statistics(test,SIZE);

}

/* Add other Implementation File Code Here */

void print_statistics(unsigned char *statArr, unsigned int statSize){ 
    sort_array(statArr,statSize);
    printf("Median = %d \n",find_median(statArr,statSize));
    printf("Mean = %d \n",find_mean(statArr,statSize));
    printf("Max. = %d \n",find_maximum(statArr,statSize));
    printf("Mini. = %d \n",find_minimum(statArr,statSize));
}

void print_array(unsigned char *arrPrint,unsigned int psize){
    for(int i=0 ; i < psize ; i++){printf("Element No. (%d) = %d \n",i,*(arrPrint+i));}
}
int sort_array(unsigned char *arrSort,unsigned int sort){
    unsigned char sorter=0;
    for(int i=0 ; i < sort ; i++){
       for(int j=i+1 ; j <sort ; j++){
         if(*(arrSort +j) > *(arrSort +i)){ 
             sorter = *(arrSort +i);
             *(arrSort +i) = *(arrSort +j);
             *(arrSort +j) = sorter;
          } 
       } 
    }

}

int find_median(unsigned char *arrMed,unsigned int med){
    int median =0;
    if(med % 2 == 0) { median = (*(arrMed + (med/2)) + *(arrMed + ((med/2)-1)))/2;}
    else median = *(arrMed + (med/2));
    return median;
}

int find_mean(unsigned char *arrMean,unsigned int mean){
    int sum = 0;  
     for(int i=0 ; i < mean ; i++){ sum = sum + *(arrMean+i);}
    sum = sum / mean;
    return sum;
}

int find_maximum(unsigned char *arrMax,unsigned int max){
    unsigned char maxn = 0;
     for(int i=0 ; i < max ; i++){
         if(*(arrMax+i) >= maxn) { maxn = *(arrMax+i);}    
            }
     return maxn;
}

int find_minimum(unsigned char *arrMini,unsigned int mini){
    unsigned char minin = 50;
     for(int i=0 ; i < mini ; i++){
         if(*(arrMini+i) < minin) { minin = *(arrMini+i);}   
            }
     return minin;
}



