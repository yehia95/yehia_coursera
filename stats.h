/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <Add File Name> 
 * @brief <Add Brief Description Here >
 *
 * <Add Extended Description Here>
 *
 * @author <Muhammad Yehia>
 * @date <7/12/2018>
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/* Add Your Declarations and Function Comments here */

void print_statistics(unsigned char *statArr, unsigned int statSize);
void print_array(unsigned char *arrPrint,unsigned int psize);
int find_median(unsigned char *arrMed,unsigned int med);
int sort_array(unsigned char *arrSort,unsigned int sort); 
int find_mean(unsigned char *arrMean,unsigned int mean);
int find_maximum(unsigned char *arrMax,unsigned int max);
int find_minimum(unsigned char *arrMini,unsigned int mini);
/**
 * @brief <Add Brief Description of Function Here>
 *
 * <Add Extended Description Here>
 *
 * @param <Add InputName> <add description here>
 * @param <Add InputName> <add description here>
 * @param <Add InputName> <add description here>
 * @param <Add InputName> <add description here>
 *
 * @return <Add Return Informaiton here>
 */


#endif /* __STATS_H__ */
